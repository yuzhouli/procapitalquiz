package edu.uchicago.cs.quiz.yli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by xxli on 7/14/13.
 */
public class ResultsActivity extends Activity {
    private static final String TAG = "ResultsActivity";


    Button mAnotherQuizButton, mResetButton;

    TextView mResultsTextView, mCorrectTextView, mIncorrectTextView, mScoreTextView;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        mResultsTextView = (TextView)findViewById(R.id.results_view);
        mResultsTextView.setText(QuizActivity.name + ", your results are:");

        //set the results
        mCorrectTextView = (TextView)findViewById(R.id.correct_view);
        mCorrectTextView.setText("correct: " + (int)QuestionActivity.numberCorrect);
        mIncorrectTextView = (TextView)findViewById(R.id.incorrect_view);
        mIncorrectTextView.setText("Incorrect: " + (int)QuestionActivity.numberIncorrect);
        mScoreTextView = (TextView)findViewById(R.id.score_view);
        mScoreTextView.setText("score: " + (int)((QuestionActivity.numberCorrect)/(QuestionActivity.numberCorrect+QuestionActivity.numberIncorrect)*100) + "%");


        //keeping the user's name and return to the quiz page but clear everything else
        mAnotherQuizButton = (Button) findViewById(R.id.btnAnotherQuiz);
        mAnotherQuizButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuestionActivity.numberIncorrect = 0;
                QuestionActivity.numberCorrect = 0;
                Log.d(TAG, "another quiz button clicked");
                Intent i = new Intent(ResultsActivity.this, QuestionActivity.class);
                Log.d(TAG, "intent created");
                startActivity(i);
            }
        });

        //bring user back to the home page to enter new name as well as clearing all results
        mResetButton = (Button) findViewById(R.id.btnReset);
        mResetButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuestionActivity.numberIncorrect = 0;
                QuestionActivity.numberCorrect = 0;
                Log.d(TAG, "reset button clicked");
                Intent i = new Intent(ResultsActivity.this, QuizActivity.class);
                Log.d(TAG, "intent created");
                startActivity(i);
            }
        });
    }
}
