package edu.uchicago.cs.quiz.yli;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by xxli on 7/14/13.
 */
public class QuestionActivity extends Activity {
    private static final String TAG = "QuestionActivity";

    Button mSubmitButton, mQuitButton;

    RadioGroup radioChoicesGroup;
    RadioButton radioSelectedButton;

    final int numOfQuestions = 15; //total number of questions
    int questionNumber = 1; //the current question number
    static double numberCorrect = 0;
    static double numberIncorrect = 0;
    int poolNumber = 0; //which question from the pool

    String selectedChoice;
    String capital;

    QuizActivity qa = new QuizActivity();

    TextView mQuestionNumberTextView, mQuestionTextView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        addContent();

        addSubmitButton();

        mQuestionNumberTextView = (TextView)findViewById(R.id.question_number_view);
        mQuestionNumberTextView.setText("Question " + questionNumber + ":");

        mQuitButton = (Button) findViewById(R.id.btnQuit);
        mQuitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "quit button clicked");
                Intent i = new Intent(QuestionActivity.this, ResultsActivity.class);
                Log.d(TAG, "intent created");
                startActivity(i);
            }
        });
    }

    //submitting the results as well as storing based on the answer chosen by the user
    public void addSubmitButton() {

        radioChoicesGroup = (RadioGroup) findViewById(R.id.choices);
        mSubmitButton = (Button) findViewById(R.id.btnSubmit);

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get selected radio button from radioGroup
                int selectedId = radioChoicesGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioSelectedButton = (RadioButton) findViewById(selectedId);
                selectedChoice = radioSelectedButton.getText().toString();
                questionNumber++;

                if(selectedChoice.equals(capital)){
                    numberCorrect++;
                } else if(!selectedChoice.equals(capital)) {
                    numberIncorrect++;
                }

                mQuestionNumberTextView.setText("Question " + questionNumber + ":");

                addContent();
            }
        });

    }

    //initialize the entire content for the page
    public void addContent() {
        Resources res = getResources();
        String[] capitalQuiz = res.getStringArray(R.array.countries_capitals);

        Random rand = new Random();

        poolNumber = rand.nextInt(numOfQuestions);

        String country = capitalQuiz[poolNumber].substring(0,capitalQuiz[poolNumber].indexOf("|"));
        capital = capitalQuiz[poolNumber].substring(capitalQuiz[poolNumber].indexOf("|")+1, capitalQuiz[poolNumber].lastIndexOf("|"));
        String region = capitalQuiz[poolNumber].substring(capitalQuiz[poolNumber].lastIndexOf("|")+1);

        TextView mAnswerTextView, mAnswer2TextView, mAnswer3TextView, mAnswer4TextView, mAnswer5TextView;
        int choiceNumber = rand.nextInt(5);

        mAnswerTextView = (TextView)findViewById(R.id.choice_1);
        mAnswer2TextView = (TextView)findViewById(R.id.choice_2);
        mAnswer3TextView = (TextView)findViewById(R.id.choice_3);
        mAnswer4TextView = (TextView)findViewById(R.id.choice_4);
        mAnswer5TextView = (TextView)findViewById(R.id.choice_5);

        String capital2 = "", capital3 = "", capital4 = "", capital5 = "";

        if(region.equals("EUR")) {
            capitalQuiz[poolNumber] = capitalQuiz[6];
            int temp = rand.nextInt(6);
            capital2 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[5];
            temp = rand.nextInt(5);
            capital3 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[4];
            temp = rand.nextInt(4);
            capital4 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[3];
            temp = rand.nextInt(3);
            capital5 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
        } else if(region.equals("ASA")) {
            capitalQuiz[poolNumber] = capitalQuiz[14];
            int temp = rand.nextInt(7)+7;
            capital2 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[13];
            temp = rand.nextInt(6)+7;
            capital3 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[12];
            temp = rand.nextInt(5)+7;
            capital4 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
            capitalQuiz[temp] = capitalQuiz[11];
            temp = rand.nextInt(4)+7;
            capital5 = capitalQuiz[temp].substring(capitalQuiz[temp].indexOf("|")+1, capitalQuiz[temp].lastIndexOf("|"));
        }


        if (choiceNumber == 0) {
            mAnswerTextView.setText(capital);
            mAnswer2TextView.setText(capital2);
            mAnswer3TextView.setText(capital3);
            mAnswer4TextView.setText(capital4);
            mAnswer5TextView.setText(capital5);
        } else if (choiceNumber == 1) {
            mAnswer2TextView.setText(capital);
            mAnswerTextView.setText(capital2);
            mAnswer3TextView.setText(capital3);
            mAnswer4TextView.setText(capital4);
            mAnswer5TextView.setText(capital5);
        } else if (choiceNumber == 2) {
            mAnswer3TextView.setText(capital);
            mAnswer2TextView.setText(capital2);
            mAnswerTextView.setText(capital3);
            mAnswer4TextView.setText(capital4);
            mAnswer5TextView.setText(capital5);
        } else if (choiceNumber == 3) {
            mAnswer4TextView.setText(capital);
            mAnswer2TextView.setText(capital2);
            mAnswer3TextView.setText(capital3);
            mAnswerTextView.setText(capital4);
            mAnswer5TextView.setText(capital5);
        } else {
            mAnswer5TextView.setText(capital);
            mAnswer2TextView.setText(capital2);
            mAnswer3TextView.setText(capital3);
            mAnswer4TextView.setText(capital4);
            mAnswerTextView.setText(capital5);
        }

        mQuestionTextView = (TextView)findViewById(R.id.question_text_view);
        mQuestionTextView.setText("What is the capital of " + country + "?");
    }
}
