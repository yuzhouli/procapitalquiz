package edu.uchicago.cs.quiz.yli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//this is my main class
public class QuizActivity extends Activity {

    private static final String TAG = "QuizActivity";

    Button mExitButton, mStartButton; //buttons on main screen
    EditText mNameEditText; //the text field where user enters name

    static String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mNameEditText = (EditText) this.findViewById(R.id.edtName);

        //buttons change
        mExitButton = (Button) findViewById(R.id.btnExit);
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitPlease();
            }

        });

        mStartButton = (Button) findViewById(R.id.btnStart);
        mStartButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                name = mNameEditText.getText().toString(); //set the name variable to the user's input
                Log.d(TAG, "start button clicked");
                Intent i = new Intent(QuizActivity.this, QuestionActivity.class);
                Log.d(TAG, "intent created");
                startActivity(i);
            }
        });
    }

    //exit the current program
    private void exitPlease() {
        finish();
    }

}
